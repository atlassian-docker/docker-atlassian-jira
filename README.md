![Atlassian Jira Software](https://wac-cdn.atlassian.com/dam/jcr:826c97dc-1f5c-4955-bfcc-ea17d6b0c095/jira%20software-icon-gradient-blue.svg?cdnVersion=492)![Atlassian Jira Service Management](https://wac-cdn.atlassian.com/dam/jcr:8e0905be-0ee7-4652-ba3a-4e3db1143969/jira%20service%20desk-icon-gradient-blue.svg?cdnVersion=492)![Atlassian Jira Core](https://wac-cdn.atlassian.com/dam/jcr:f89f1ce5-60f1-47c2-b9f5-657de4940d31/jira%20core-icon-gradient-blue.svg?cdnVersion=492)

Jira Software Data Center helps the world’s best agile teams plan, track, and release great software at scale.

* Check out [atlassian/jira-software](http://hub.docker.com/r/atlassian/jira-software/) on Docker Hub
* Learn more about Jira Software: [https://www.atlassian.com/software/jira](https://www.atlassian.com/software/jira)

Jira Service Management Data Center is an enterprise ITSM solution that offers high availability, meeting your security and compliance needs so no request goes unresolved.

* Check out [atlassian/jira-servicemanagement](http://hub.docker.com/r/atlassian/jira-servicemanagement/) on Docker Hub
* Learn more about Jira Service Management: [https://www.atlassian.com/software/jira/service-management](https://www.atlassian.com/software/jira/service-management)

Jira Core is a project and task management solution built for business teams.

* Check out [atlassian/jira-core](http://hub.docker.com/r/atlassian/jira-core/) on Docker Hub
* Learn more about Jira Core: [https://www.atlassian.com/software/jira/core](https://www.atlassian.com/software/jira/core)

# Overview

This Docker container makes it easy to get an instance of Jira Software, Service Management or Core up and running.

**Note**: Jira Software will be referenced in the examples provided.

**Use docker version >= 20.10.10**

# Quick Start

For the `JIRA_HOME` directory that is used to store application data (amongst
other things) we recommend mounting a host directory as a [data
volume](https://docs.docker.com/engine/tutorials/dockervolumes/#/data-volumes),
or via a named volume.

Additionally, if running Jira in Data Center mode it is required that a shared
filesystem is mounted. The mountpoint (inside the container) can be configured
with `JIRA_SHARED_HOME`.

To get started you can use a data volume, or named volumes. In this example
we'll use named volumes.

    docker volume create --name jiraVolume
    docker run -v jiraVolume:/var/atlassian/application-data/jira --name="jira" -d -p 8080:8080 atlassian/jira-software


**Success**. Jira is now available on [http://localhost:8080](http://localhost:8080)*

Please ensure your container has the necessary resources allocated to it. We
recommend 2GiB of memory allocated to accommodate the application server. See
[System Requirements](https://confluence.atlassian.com/adminjiraserver071/jira-applications-installation-requirements-802592164.html)
for further information.


_* Note: If you are using `docker-machine` on Mac OS X, please use `open
http://$(docker-machine ip default):8080` instead._

# Advanced Usage
For advanced usage, e.g. configuration, troubleshooting, supportability, etc.,
please check the [**Full Documentation**](https://atlassian.github.io/data-center-helm-charts/containers/JIRA/).

