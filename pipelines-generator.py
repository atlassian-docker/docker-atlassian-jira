
from pathlib import Path
import os
import jinja2 as j2

TEMPLATE_FILE = 'bitbucket-pipelines.yml.j2'

DOCKERFILE_VERSION_ARG='JIRA_VERSION'

CORE_REPOS = ['atlassian/jira-core']
SOFTWARE_REPOS = ['atlassian/jira-software']
SD_REPOS = ['atlassian/jira-servicemanagement']

images = {
    'Jira Software': {
        "11-default": {
            'mac_key': 'jira-software',
            'artefact': 'atlassian-jira-software',
            'start_version': '9.7',
            'end_version': '9.11',
            'default_release': True,
            'tag_suffixes': ['jdk11', 'ubuntu-jdk11'],
            'base_image': 'eclipse-temurin:11-noble',
            'dockerfile': 'Dockerfile',
            'docker_repos': SOFTWARE_REPOS,
            'batches': 6
        },
        11: {
            'mac_key': 'jira-software',
            'artefact': 'atlassian-jira-software',
            'start_version': '9.11',
            'end_version': '10',
            'tag_suffixes': ['jdk11', 'ubuntu-jdk11'],
            'base_image': 'eclipse-temurin:11-noble',
            'dockerfile': 'Dockerfile',
            'docker_repos': SOFTWARE_REPOS,
            'batches': 6
        },
        "17-default": {
            'mac_key': 'jira-software',
            'artefact': 'atlassian-jira-software',
            'start_version': '9.11',
            'default_release': True,
            'tag_suffixes': ['jdk17', 'ubuntu-jdk17'],
            'base_image': 'eclipse-temurin:17-noble',
            'dockerfile': 'Dockerfile',
            'docker_repos': SOFTWARE_REPOS,
            'batches': 4
        },
        17: {
            'mac_key': 'jira-software',
            'artefact': 'atlassian-jira-software',
            'start_version': '9.5',
            'end_version': '9.11',
            'tag_suffixes': ['jdk17', 'ubuntu-jdk17'],
            'base_image': 'eclipse-temurin:17-noble',
            'dockerfile': 'Dockerfile',
            'docker_repos': SOFTWARE_REPOS,
            'batches': 4
        },
        "17-ubi": {
            'mac_key': 'jira-software',
            'artefact': 'atlassian-jira-software',
            'start_version': '9.5',
            'tag_suffixes': ['ubi9','ubi9-jdk17'],
            'base_image': 'registry.access.redhat.com/ubi9/openjdk-17',
            'dockerfile': 'Dockerfile.ubi',
            'docker_repos': SOFTWARE_REPOS,
            'batches': 8,
            'snyk_threshold': 'critical'
        }
    },
    'Jira Service Management': {
        "11-default": {
            'mac_key': 'jira-servicedesk',
            'artefact': 'atlassian-servicedesk',
            'start_version': '5.7',
            'end_version': '5.11',
            'default_release': True,
            'tag_suffixes': ['jdk11', 'ubuntu-jdk11'],
            'base_image': 'eclipse-temurin:11-noble',
            'dockerfile': 'Dockerfile',
            'docker_repos': SD_REPOS,
            'batches': 4
        },
        11: {
            'mac_key': 'jira-servicedesk',
            'artefact': 'atlassian-servicedesk',
            'start_version': '5.11',
            'end_version': '6.0',
            'tag_suffixes': ['jdk11', 'ubuntu-jdk11'],
            'base_image': 'eclipse-temurin:11-noble',
            'dockerfile': 'Dockerfile',
            'docker_repos': SD_REPOS,
            'batches': 4
        },
        "17-default": {
            'mac_key': 'jira-servicedesk',
            'artefact': 'atlassian-servicedesk',
            'start_version': '5.11',
            'default_release': True,
            'tag_suffixes': ['jdk17', 'ubuntu-jdk17'],
            'base_image': 'eclipse-temurin:17-noble',
            'dockerfile': 'Dockerfile',
            'docker_repos': SD_REPOS,
            'batches': 4
        },
        17: {
            'mac_key': 'jira-servicedesk',
            'artefact': 'atlassian-servicedesk',
            'start_version': '5.5',
            'end_version': '5.11',
            'tag_suffixes': ['jdk17', 'ubuntu-jdk17'],
            'base_image': 'eclipse-temurin:17-noble',
            'dockerfile': 'Dockerfile',
            'docker_repos': SD_REPOS,
            'batches': 4
        },
        "17-ubi": {
            'mac_key': 'jira-servicedesk',
            'artefact': 'atlassian-servicedesk',
            'start_version': '5.5',
            'tag_suffixes': ['ubi9','ubi9-jdk17'],
            'base_image': 'registry.access.redhat.com/ubi9/openjdk-17',
            'dockerfile': 'Dockerfile.ubi',
            'docker_repos': SD_REPOS,
            'batches': 8,
            'snyk_threshold': 'critical'
        }
    },
    'Jira Core': {
        "11-default": {
            'mac_key': 'jira',
            'artefact': 'atlassian-jira-core',
            'start_version': '9.7',
            'end_version': '9.11',
            'default_release': True,
            'tag_suffixes': ['jdk11', 'ubuntu-jdk11'],
            'base_image': 'eclipse-temurin:11-noble',
            'dockerfile': 'Dockerfile',
            'docker_repos': CORE_REPOS,
            'batches': 6
        },
        11: {
            'mac_key': 'jira',
            'artefact': 'atlassian-jira-core',
            'start_version': '9.11',
            'end_version': '10',
            'tag_suffixes': ['jdk11', 'ubuntu-jdk11'],
            'base_image': 'eclipse-temurin:11-noble',
            'dockerfile': 'Dockerfile',
            'docker_repos': CORE_REPOS,
            'batches': 6
        },
        "17-default": {
            'mac_key': 'jira',
            'artefact': 'atlassian-jira-core',
            'start_version': '9.11',
            'default_release': True,
            'tag_suffixes': ['jdk17', 'ubuntu-jdk17'],
            'base_image': 'eclipse-temurin:17-noble',
            'dockerfile': 'Dockerfile',
            'docker_repos': CORE_REPOS,
            'batches': 4
        },
        17: {
            'mac_key': 'jira',
            'artefact': 'atlassian-jira-core',
            'start_version': '9.5',
            'end_version': '9.11',
            'tag_suffixes': ['jdk17', 'ubuntu-jdk17'],
            'base_image': 'eclipse-temurin:17-noble',
            'dockerfile': 'Dockerfile',
            'docker_repos': CORE_REPOS,
            'batches': 4
        },
        "17-ubi": {
            'mac_key': 'jira',
            'artefact': 'atlassian-jira-core',
            'start_version': '9.5',
            'tag_suffixes': ['ubi9','ubi9-jdk17'],
            'base_image': 'registry.access.redhat.com/ubi9/openjdk-17',
            'dockerfile': 'Dockerfile.ubi',
            'docker_repos': CORE_REPOS,
            'batches': 8,
            'snyk_threshold': 'critical'
        }
    }
}


def main():
    jenv = j2.Environment(
        loader=j2.FileSystemLoader('.'),
        lstrip_blocks=True,
        trim_blocks=True)
    template = jenv.get_template(TEMPLATE_FILE)
    generated_output = template.render(images=images, batches=12)

    print(generated_output)

if __name__ == '__main__':
    main()
